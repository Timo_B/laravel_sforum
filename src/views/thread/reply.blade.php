
<section id="maincontent">
    <div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
					<h2><a href="{{ URL::to('thread/view/'.$thread->id) }}">{{ $thread->topic }}</a></h2>
					<div class="thread-row">
						<div class="user-info" style="float:left;">
							{{ $user->username }}
						</div>
						<div class="post-text" style="float:left;padding-left:10px;">
							<p>{{ $thread->text }}</p>
						</div>
					</div>
					

				
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				@if($errors->has())
					<ul>
						@foreach($errors->all() as $message)
							<li>{{ $message }}</li>
						@endforeach
					</ul>
				@endif

				{{ Form::open(array('url'=>'thread/reply/'.$thread->id, 'class'=>'form-signin', 'role'=>"form")) }}
					<div class="row">
						<div class="form-group col-xs-12 floating-label-form-group">
							<label for="name">Topic</label>
				    		{{ Form::text('topic', null, array('class'=>'input-block-level', 'placeholder'=>'Topic')) }}
				    	</div>
				    </div>
				    <div class="row">
						<div class="form-group col-xs-12 floating-label-form-group">
							<label for="name">Text</label>
				    		{{ Form::textarea('posttext', null, array('class'=>'input-block-level')) }}
				    	</div>
				    </div>
				    
				    <div class="row">
				    	<div class="form-group col-xs-12">
				    		{{ Form::submit('Save', array('class'=>'btn btn-lg btn-success'))}}
				    	</div>
				    </div>
				{{ Form::close() }}
			</div>
		</div>




		

		

		 
	</div>
</section>