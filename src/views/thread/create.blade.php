
<section id="maincontent">
    <div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
		    	<h2>Create topic</h2>
		        <hr class="star-primary">
			</div>
		</div>
		<div class="row">

			<div class="col-lg-8 col-lg-offset-2">
				@if($errors->has())
					<ul>
						@foreach($errors->all() as $message)
							<li>{{ $message }}</li>
						@endforeach
					</ul>
				@endif
				<?php
				Input::old('posttext', 'aaa');
				 ?>
				{{ Form::open(array('url'=>'thread/create/'.$forum->id, 'class'=>'form-signin', 'role'=>"form")) }}
				<div class="row">
					<div class="form-group col-xs-12 floating-label-form-group">
						<label for="name">Topic</label>
			    		{{ Form::text('topic', null, array('class'=>'input-block-level', 'placeholder'=>'Topic')) }}
			    	</div>
			    </div>
			    <div class="row">
					<div class="form-group col-xs-12 floating-label-form-group">
						<label for="name">Topic</label>
{{ var_dump(Purifier::clean(Input::old('posttext'))) }}
			    		<textarea name="posttext" class="input-block-level" id="summernote">{{ (Input::old('posttext')) ? Purifier::clean(Input::old('posttext')) : '' }}</textarea>
			    	</div>
			    </div>
			    
			    <div class="row">
			    	<div class="form-group col-xs-12">
			    		{{ Form::submit('Save', array('class'=>'btn btn-lg btn-success'))}}
			    	</div>
			    </div>
		{{ Form::close() }}

			</div>
		</div>

		
		 
	</div>
</section>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script> 
<link  rel="stylesheet" type="text/css"  href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"> 
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> 
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<script type="text/javascript" src="{{ URL::asset('summernote/summernote.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('summernote/summernote.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('summernote/summernote-bs3.css') }}">

<script type="text/javascript">
$(document).ready(function() {
  $('#summernote').summernote();
});
</script>