
<section id="maincontent">
    <div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				@if ($writeAccess)
					<p><a href="{{ URL::to('thread/reply/'.$thread->id) }}">Reply</a></p>
				@endif
				
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
					<h2><a href="{{ URL::to('thread/view/'.$thread->id) }}">{{ $thread->topic }}</a></h2>
					<div class="thread-row">
						<div class="user-info" style="float:left;">
							{{ $user->username }}
						</div>
						<div class="post-text" style="float:left;padding-left:10px;">
							<p>{{ $thread->text }}</p>
						</div>
					</div>
					

				
			</div>
		</div>

		@foreach($replies as $reply)
			
			<div class="row" style="clear:both;">
				<h3>{{ $reply->topic }}</h3>
				<div class="thread-row">
					<div class="user-info" style="float:left;">
						{{ $reply->user->username }}
					</div>
					<div class="post-text" style="float:left;padding-left:10px;">
						<p>{{ $reply->text }}</p>
					</div>
				</div>
			</div>
		@endforeach
		<div class="row">
			{{ $replies->links() }}
		</div>

		 
	</div>
</section>