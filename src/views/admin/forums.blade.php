
<section id="maincontent">
    <div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
		    	<h2>Forumgroups</h2>
		        <hr class="star-primary">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				@if($errors->has())
					<ul>
						@foreach($errors->all() as $message)
							<li>{{ $message }}</li>
						@endforeach
					</ul>
				@endif
			
				{{ Form::open(array('url'=>'admin/forums', 'class'=>'form-signin', 'role'=>"form")) }}
				<div class="row">
					<div class="form-group col-xs-12 floating-label-form-group">
						<label for="name">Forumgroup name</label>
			    		{{ Form::text('name', null, array('class'=>'input-block-level', 'placeholder'=>'Name')) }}
			    	</div>
			    </div>
			    <div class="row">
					<div class="form-group col-xs-12 floating-label-form-group">
						<label for="name">Groups (read)</label>

			    		{{ Form::select('rgroups[]', $groups, '', array('multiple'))  }}
			    	</div>
			    </div>
			    <div class="row">
					<div class="form-group col-xs-12 floating-label-form-group">
						<label for="name">Groups (write)</label>

			    		{{ Form::select('wgroups[]', $groups, '', array('multiple'))  }}
			    	</div>
			    </div>
			    <div class="row">
					<div class="form-group col-xs-12 floating-label-form-group">
						<label for="name">Forumgroup</label>

			    		{{ Form::select('fgroup', $forumgroups, '')  }}
			    	</div>
			    </div>
			    <div class="row">
			    	<div class="form-group col-xs-12">
			    		{{ Form::submit('Save', array('class'=>'btn btn-lg btn-success'))}}
			    	</div>
			    </div>
		{{ Form::close() }}

			</div>
		</div>

		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				@foreach ($forums as $f)
				    <p>{{ $f->name }} </p>
				@endforeach
			</div>
		</div>
		 
	</div>
</section>

