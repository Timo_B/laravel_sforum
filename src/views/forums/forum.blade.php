
<section id="maincontent">
    <div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				@if ($writeAccess)
					<p><a href="{{ URL::to('thread/create/'.$forum->id) }}">Create new thread</a></p>
				@endif
				
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				@foreach ($threads as $thread)
					<p><a href="{{ URL::to('thread/view/'.$thread->id) }}">{{ $thread->topic }}</a></p>
				@endforeach
				
			</div>
		</div>
		 
	</div>
</section>

