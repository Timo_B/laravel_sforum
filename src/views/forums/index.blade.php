
<section id="maincontent">
    <div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				@foreach ($forums as $f)
				    <p>{{ $f['name'] }}</p>
				    @foreach ($f['forums'] as $forum)
				    	<p><a href="{{ URL::to('forum/'.$forum->id) }}">{{ $forum->name }}</a></p>
				    @endforeach

				@endforeach
			</div>
		</div>
		 
	</div>
</section>

