<?php namespace Timob\Sforum\controllers;

use \Illuminate\Routing\Controllers\Controller;
use \BaseController;
use \View;
use \Validator;
use \Input;
use \Redirect;
use \User;
use \Hash;
use \Verification;
use \Mail;
use \Auth;
use \Config;
use \Forumgroups;
use \Forums;
use \Threads;
use \Replies;
use \DB;
use \Purifier;



class SforumController extends BaseController {

	public function __construct()
	{
		$this->userGroups = \Timob\Sforum\controllers\HelperController::getUserGroups();
		
	}

	public function postReplyView($id='')
	{
		$thread = Threads::where('id', '=', $id)->first();

		$forum = Forums::where('id', '=', $thread->forum)->first();
		$forumgroup = Forumgroups::where('id', '=', $forum->forumgroups)->first();
		
		$isAllowed = 0;
		foreach($this->userGroups as $group){
			if(in_array($group, explode(',',$forumgroup->groups))){
				if(in_array($group, explode(',',$forum->readgroups)) OR in_array($group, explode(',',$forum->writegroups))){
					$isAllowed++;
				} elseif($forum->readgroups == '') {
					$isAllowed++;
				}
			} elseif ($forumgroup->groups == '') {
				if(in_array($group, explode(',',$forum->readgroups)) OR in_array($group, explode(',',$forum->writegroups))){
					$isAllowed++;
				}
			} elseif($forumgroup->groups == '' AND $forum->readgroups == ''){
				$isAllowed++;
			} 
		}
		if(!$this->userGroups){
			if($forumgroup->groups == '' AND $forum->readgroups == ''){
				$isAllowed++;
			}
		}
		if($forumgroup->groups == '' AND $forum->readgroups == ''){
			$isAllowed++;
		}
		if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
			$isAllowed++;
		}

		if($isAllowed > 0){
			$writeAccess = false;
			foreach($this->userGroups as $group){
				if(in_array($group, explode(',',$forum->writegroups))){
					$writeAccess = true; 
				}
			}
			if($forum->writegroups == '' && Auth::check()){
				$writeAccess = true;
			}
			
			if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
				$writeAccess  = true;
			}
		} else {
			return Redirect::to('/login')->with('message', 'Missing permissions');
		}

		$rules = array(
		    'topic'=>'required|min:5',
		    'posttext' => 'required'
		);
	    
	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->passes()) {
        	$reply = new Replies;
		    $reply->topic = Purifier::clean(Input::get('topic'));
		    $reply->text = Purifier::clean(Input::get('posttext'));
		    $reply->user = Auth::user()->id;
		    $reply->thread = $thread->id;
		    
		    $reply->save();




		    return Redirect::to('thread/view/'.$thread->id)->with('message', 'Created!');
    	} else {
        	return Redirect::to('thread/reply/'.$thread->id)->with('message', 'The following errors occurred')->withErrors($validator)->withInput();   
    	}

	}


	public function getReplyView($id='')
	{
		$thread = Threads::where('id', '=', $id)->first();

		$forum = Forums::where('id', '=', $thread->forum)->first();
		$forumgroup = Forumgroups::where('id', '=', $forum->forumgroups)->first();
		
		$isAllowed = 0;
		foreach($this->userGroups as $group){
			if(in_array($group, explode(',',$forumgroup->groups))){
				if(in_array($group, explode(',',$forum->readgroups)) OR in_array($group, explode(',',$forum->writegroups))){
					$isAllowed++;
				} elseif($forum->readgroups == '') {
					$isAllowed++;
				}
			} elseif ($forumgroup->groups == '') {
				if(in_array($group, explode(',',$forum->readgroups)) OR in_array($group, explode(',',$forum->writegroups))){
					$isAllowed++;
				}
			} elseif($forumgroup->groups == '' AND $forum->readgroups == ''){
				$isAllowed++;
			} 
		}
		if(!$this->userGroups){
			if($forumgroup->groups == '' AND $forum->readgroups == ''){
				$isAllowed++;
			}
		}
		if($forumgroup->groups == '' AND $forum->readgroups == ''){
			$isAllowed++;
		}
		if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
			$isAllowed++;
		}

		if($isAllowed > 0){
			$writeAccess = false;
			foreach($this->userGroups as $group){
				if(in_array($group, explode(',',$forum->writegroups))){
					$writeAccess = true; 
				}
			}
			if($forum->writegroups == '' && Auth::check()){
				$writeAccess = true;
			}
			
			if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
				$writeAccess  = true;
			}
		} else {
			return Redirect::to('/login')->with('message', 'Missing permissions');
		}
		$user = User::where('id', '=', $thread->user)->first();

		return View::make('sforum::thread.reply')->with(array('thread' => $thread, 'user' => $user));


	}

	public function getThreadView($id='')
	{
		$thread = Threads::where('id', '=', $id)->first();

		$forum = Forums::where('id', '=', $thread->forum)->first();
		$forumgroup = Forumgroups::where('id', '=', $forum->forumgroups)->first();
		
		$isAllowed = 0;
		foreach($this->userGroups as $group){
			if(in_array($group, explode(',',$forumgroup->groups))){
				if(in_array($group, explode(',',$forum->readgroups)) OR in_array($group, explode(',',$forum->writegroups))){
					$isAllowed++;
				} elseif($forum->readgroups == '') {
					$isAllowed++;
				}
			} elseif ($forumgroup->groups == '') {
				if(in_array($group, explode(',',$forum->readgroups)) OR in_array($group, explode(',',$forum->writegroups))){
					$isAllowed++;
				}
			} elseif($forumgroup->groups == '' AND $forum->readgroups == ''){
				$isAllowed++;
			} 
		}
		if(!$this->userGroups){
			if($forumgroup->groups == '' AND $forum->readgroups == ''){
				$isAllowed++;
			}
		}
		if($forumgroup->groups == '' AND $forum->readgroups == ''){
			$isAllowed++;
		}
		if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
			$isAllowed++;
		}

		if($isAllowed > 0){
			$writeAccess = false;
			foreach($this->userGroups as $group){
				if(in_array($group, explode(',',$forum->writegroups))){
					$writeAccess = true; 
				}
			}
			if($forum->writegroups == '' && Auth::check()){
				$writeAccess = true;
			}
			
			if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
				$writeAccess  = true;
			}
		} else {
			return Redirect::to('/login')->with('message', 'Missing permissions');
		}

		$user = User::where('id', '=', $thread->user)->first();
		$replies = Replies::where('thread', '=' , $thread->id)->paginate(10);

		foreach ($replies as $reply) {
			
			$reply->user = User::where('id', '=', $reply->user)->first();
		}

		return View::make('sforum::thread.view')->with(array('thread' => $thread, 'writeAccess' => $writeAccess, 'user' => $user, 'replies' => $replies));

	}

	public function postThreadCreate($id='')
	{
		$forum = Forums::where('id', '=', $id)->first();
		$forumgroup = Forumgroups::where('id', '=', $forum->forumgroups)->first();
		$writeAccess = false;
		foreach($this->userGroups as $group){
			if(in_array($group, explode(',',$forum->writegroups))){
				$writeAccess = true; 
			}
		}
		if($forum->writegroups == '' && Auth::check()){
			$writeAccess = true;
		}
		
		if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
			$writeAccess  = true;
		}

		if(!$writeAccess){
			return Redirect::to('/login')->with('message', 'Missing permissions');
		}



		$rules = array(
		    'topic'=>'required|min:5',
		    'posttext' => 'required'
		);
	    
	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->passes()) {
        	$thread = new Threads;
		    $thread->topic = Purifier::clean(Input::get('topic'));
		    $thread->text = Purifier::clean(Input::get('posttext'));
		    $thread->user = Auth::user()->id;
		    $thread->forum = $forum->id;
		    
		    $thread->save();




		    return Redirect::to('forum/'.$forum->id)->with('message', 'Created!');
    	} else {
        	return Redirect::to('thread/create/'.$forum->id)->with('message', 'The following errors occurred')->withErrors($validator)->withInput();   
    	}


	}

	public function getThreadCreate($id='')
	{
		$forum = Forums::where('id', '=', $id)->first();
		$forumgroup = Forumgroups::where('id', '=', $forum->forumgroups)->first();
		$writeAccess = false;
		foreach($this->userGroups as $group){
			if(in_array($group, explode(',',$forum->writegroups))){
				$writeAccess = true; 
			}
		}
		if($forum->writegroups == '' && Auth::check()){
			$writeAccess = true;
		}
		
		if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
			$writeAccess  = true;
		}

		if(!$writeAccess){
			return Redirect::to('/login')->with('message', 'Missing permissions');
		}

		return View::make('sforum::thread.create')->with(array('forum' => $forum));

		
	}

	public function getForum($id='')
	{
		$forum = Forums::where('id', '=', $id)->first();
		$forumgroup = Forumgroups::where('id', '=', $forum->forumgroups)->first();
		
		$isAllowed = 0;
		foreach($this->userGroups as $group){
			if(in_array($group, explode(',',$forumgroup->groups))){
				if(in_array($group, explode(',',$forum->readgroups)) OR in_array($group, explode(',',$forum->writegroups))){
					$isAllowed++;
				} elseif($forum->readgroups == '') {
					$isAllowed++;
				}
			} elseif ($forumgroup->groups == '') {
				if(in_array($group, explode(',',$forum->readgroups)) OR in_array($group, explode(',',$forum->writegroups))){
					$isAllowed++;
				}
			} elseif($forumgroup->groups == '' AND $forum->readgroups == ''){
				$isAllowed++;
			} 
		}
		if(!$this->userGroups){
			if($forumgroup->groups == '' AND $forum->readgroups == ''){
				$isAllowed++;
			}
		}
		if($forumgroup->groups == '' AND $forum->readgroups == ''){
			$isAllowed++;
		}
		if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
			$isAllowed++;
		}

		if($isAllowed > 0){
			$writeAccess = false;
			foreach($this->userGroups as $group){
				if(in_array($group, explode(',',$forum->writegroups))){
					$writeAccess = true; 
				}
			}
			if($forum->writegroups == '' && Auth::check()){
				$writeAccess = true;
			}
			
			if(in_array(Config::get('sforum::adminGroupId'),$this->userGroups)){
				$writeAccess  = true;
			}
		} else {
			return Redirect::to('/login')->with('message', 'Missing permissions');
		}

		$threads = Threads::where('forum', '=', $forum->id)->get();

		return View::make('sforum::forums.forum')->with(array('writeAccess' => $writeAccess, 'forum' => $forum, 'threads' => $threads));


	}
	
	public function getForums()
	{
		

		$fg = Forumgroups::where('groups', '=', '');
		foreach ($this->userGroups as $usergroup) {
			$fg = $fg->orWhereRaw('FIND_IN_SET('.intval($usergroup).', `groups`)');
		}
		$fg = $fg->get();

		$forum = array();

		foreach($fg as $forumgroup){
			$forums = Forums::where('forumgroups', '=', $forumgroup->id);
			$sql = '( `readgroups` = \'\'';
			foreach ($this->userGroups as $usergroup) {
				$sql .= ' OR FIND_IN_SET('.intval($usergroup).', `readgroups`) OR FIND_IN_SET('.intval($usergroup).', `writegroups`)';
			}
			$sql .= ')';
			$forums = $forums->whereRaw($sql);
			$forums = $forums->get();
			if(count($forums)){
				$forum[$forumgroup->id] = array(
					'name' => $forumgroup->name,
					'forums' => $forums
				);
			}
			
			
		}

		if(count($forum)){
			
		} else {
			echo 'Nothing yet! Login?';
		}

		return View::make('sforum::forums.index')->with(array('forums' => $forum));
		

	}

	public function postAdminForums()
	{
		if(!in_array(Config::get('sforum::adminGroupId'), $this->userGroups)){
			return Redirect::to(Config::get('sforum::loginRoute'));
		}


		$rules = array(
		    'name'=>'required|min:2',
		    'fgroup' => 'required'
		);
	    
	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->passes()) {
        	$forum = new Forums;
		    $forum->name = Input::get('name');
		    $forum->readgroups = (Input::get('rgroups')) ? implode(',',Input::get('rgroups')) : '';
		    $forum->writegroups = (Input::get('wgroups')) ? implode(',',Input::get('wgroups')) : '';
		    $forum->forumgroups = (Input::get('fgroup')) ? Input::get('fgroup') : '';
		    $forum->save();




		    return Redirect::to('admin/forums')->with('message', 'Created!');
    	} else {
        	return Redirect::to('admin/forumgroups')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();   
    	}
	}

	public function getAdminForums()
	{
		
		if(!in_array(Config::get('sforum::adminGroupId'), $this->userGroups)){
			return Redirect::to(Config::get('sforum::loginRoute'));
		}

		$forums = Forums::all();
		$forumgroups = Forumgroups::all();

		$fgroups = array();
		foreach ($forumgroups as $fgroup) {
			$fgroups[$fgroup->id] = $fgroup->name;
		}

		$groups = \Timob\Sforum\controllers\HelperController::getGroups();
		return View::make('sforum::admin.forums')->with(array('groups' => $groups, 'forums' => $forums, 'forumgroups' => $fgroups));
	}

	public function getForumGroups()
	{
			
		#$userGroups =  \Timob\Sforum\controllers\HelperController::getUserGroups();
		#if(!in_array(Config::get('sforum::adminGroupId'), $userGroups)){
		if(!in_array(Config::get('sforum::adminGroupId'), $this->userGroups)){
			return Redirect::to(Config::get('sforum::loginRoute'));
		}

		$forumgroups = Forumgroups::all();

		$groups = \Timob\Sforum\controllers\HelperController::getGroups();
		return View::make('sforum::admin.forumgroups')->with(array('groups' => $groups, 'forumgroups' => $forumgroups));
		
	}

	public function postForumGroups()
	{
		if(!in_array(Config::get('sforum::adminGroupId'), $this->userGroups)){
			return Redirect::to(Config::get('sforum::loginRoute'));
		}

		$rules = array(
		    'name'=>'required|min:2'
		);
	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->passes()) {
        	$forumgroups = new Forumgroups;
		    $forumgroups->name = Input::get('name');

		    $forumgroups->groups = (Input::get('groups')) ? implode(',',Input::get('groups')) : '';
		    $forumgroups->save();




		    return Redirect::to('admin/forumgroups')->with('message', 'Created!');
    	} else {
        	return Redirect::to('admin/forumgroups')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();   
    	}
	}

	
}