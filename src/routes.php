<?php 

Route::get('admin/forumgroups', array('uses' => 'Timob\Sforum\controllers\SforumController@getForumGroups'));
Route::post('admin/forumgroups', array('uses' => 'Timob\Sforum\controllers\SforumController@postForumGroups'));


Route::get('admin/forums', array('uses' => 'Timob\Sforum\controllers\SforumController@getAdminForums'));
Route::post('admin/forums', array('uses' => 'Timob\Sforum\controllers\SforumController@postAdminForums'));

Route::get('/forums', array('uses' => 'Timob\Sforum\controllers\SforumController@getForums'));
Route::get('/forum/{id}', array('uses' => 'Timob\Sforum\controllers\SforumController@getForum'));

Route::get('/thread/create/{id}', array('uses' => 'Timob\Sforum\controllers\SforumController@getThreadCreate'));
Route::post('/thread/create/{id}', array('uses' => 'Timob\Sforum\controllers\SforumController@postThreadCreate'));
Route::get('/thread/view/{id}', array('uses' => 'Timob\Sforum\controllers\SforumController@getThreadView'));

Route::get('/thread/reply/{id}', array('uses' => 'Timob\Sforum\controllers\SforumController@getReplyView'));
Route::post('/thread/reply/{id}', array('uses' => 'Timob\Sforum\controllers\SforumController@postReplyView'));


Route::get('admin/hashpw', function()
{
    echo Hash::make('asdf');
});